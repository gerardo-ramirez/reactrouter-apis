import React, {  Component } from 'react';
import { Link } from 'react-router-dom';
class Navbar extends Component{
  render(){
    return(
      <div>
      <ul>
      <li>
      <Link to='/apiComments'> Comments</ Link>
      </li>
      <li>
      <Link to='/apiStarwar'> Start war </ Link>
      </li>
      <li>
      <Link to='/about'> about </ Link>
      </li>
      <li>
      <Link to='/contact'> contact </ Link>
      </li>
      <li>
      <Link to='/ApiUser'> ApiUser </ Link>
      </li>
      <li>
      <Link to='/ApiTodos'> ApiTodos </ Link>
      </li>
      <li>
      <Link to='/ApiPosts'> ApiPosts</ Link>
      </li>
      <li>
      <Link to='/'> Home </ Link>
      </li>

      </ul>
      </div>
    )
  }
}

export default Navbar
