import React, { Component } from 'react';
class ApiComments extends Component{
  constructor(){
    super();
    this.state={
      comments:[]
    }
  };
  componentDidMount(){
    fetch('https://jsonplaceholder.typicode.com/comments/')
    .then(res => res.json())
    .then(comments => this.setState({comments}))
  }
  render(){
    return(
      <div>
      <h3>Comentarios</h3>
      <ul>
      {this.state.comments.map(comment => {
        return (
          <div key={comment.id}>
          <li> {comment.name} </li>
          <p> {comment.body} </p>
          </div>
        )
      })}

      </ul>

      </div>
    )
  }

}
export default ApiComments;
