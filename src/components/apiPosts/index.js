import React, { Component } from 'react';

class ApiPosts extends Component{
  constructor(){
    super();
    this.state = {
      posts:[],
      load: true,
    }
  };
  async componentDidMount(){
    await fetch('https://jsonplaceholder.typicode.com/posts').then(res => res.json())
    .then(response => this.setState({
      posts: response,
      load: false,
    }))
  };
  getPosts(){
    const { posts } = this.state;
    return(
      posts.map(post => (
        <li key={post.id}>
        {post.title}
        </li>
      ))
    )
  };

  render(){
    const { load } = this.state;
    return(
      <div>
      <h2>Api Posts</h2>
      {!load ?
        <ul>
        {this.getPosts()}
        </ul> : <p>Cargando...</p>

      }

      </div>
    )
  }

}
export default ApiPosts;
