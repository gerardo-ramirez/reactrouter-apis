import React, { Component } from 'react';

class ApiTodos extends Component{
  constructor(){
    super();
    this.state = {
      todos:[],
      isLoaded:false,
    }
  }

async componentDidMount(){
const URL = "https://jsonplaceholder.typicode.com/todos";
await fetch(URL).then(response => response.json())
.then(res => this.setState({
  todos:res,
  isLoaded: true,
})) ;
console.log(this.state.todos);
}

getTodos = () =>{
  const { todos , isLoaded } = this.state;

        return(
          todos.map(data => (
            <li key={data.id}>
            Name: {data.title}
            </li>))
              )


};


  render(){
    return(
      <div>
        <h2> Api todos </h2>
        <ul>p
        {this.getTodos()}
        </ul>
      </div>
    )
  }
}

export default ApiTodos ;
