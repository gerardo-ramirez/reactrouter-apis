import React, { Component } from 'react';

class ConsumoApi extends Component{
        constructor(){
          super();
          this.state = {
            isLoaded: false,
            item:[],
          }
        };
  async componentDidMount(){
    await fetch('https://jsonplaceholder.typicode.com/users')
    .then(res => res.json())
    .then(data => this.setState({
      item : data,
      isLoaded:true,
    }))
    console.log(this.state.item);
    console.log(this.state.item[1].name);
  };
getData = () => {
  let {item , isLoaded } = this.state;


    if(!isLoaded){
      return <div>loading...</div>;
    }
    else{
      return(
      <div>
      <h2>Aqui se consume una api Users</h2>
      <ul>
        {item.map(data => (
          <li key={data.id}>
          Name: {data.name} || email:{data.email}
          </li>
        ))}
      </ul>
      </div>
        )
    }



};



  render() {
    return (
      <div>{this.getData()} </div>
    )

}
};
export default ConsumoApi;
