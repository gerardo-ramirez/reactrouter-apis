import React from 'react';
import {Switch, Route  } from 'react-router-dom';
import Contact  from '../components/contact/Contact.js'
import About  from '../components/about/About.js';
import Home from '../components/home/Home.js';
import Page404 from '../components/page404/Page404.js';
import ConsumoApi from '../components/consumoApi';
import ApiTodos from '../components/apiTodos';
import ApiPosts from '../components/apiPosts';
import ApiStarwar from '../components/apiStarwar';
import ApiComments from '../components/apiComments';





const Rutas = ()=>{
  return(
    <Switch>
    <Route exact path="/about" component={About}/>
    <Route exact path="/contact" component={Contact}/>
    <Route exact path="/ApiUser" component={ConsumoApi}/>
    <Route exact path="/ApiTodos" component={ApiTodos}/>
    <Route exact path="/ApiPosts" component={ApiPosts}/>
    <Route exact path="/apiStarwar" component={ApiStarwar}/>
    <Route exact path="/apiComments" component={ApiComments}/>



    <Route exact path="/" component={Home}/>
    <Route component={Page404}/>



    </Switch>
  )
}
export default Rutas;
